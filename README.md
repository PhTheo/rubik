# Rubik'

# Face numbering

```
                     +--------------+
                     |              |
                     |  1    2    3 |
                     |              |
                     |  4  top    5 |
                     |              |
                     |  6    7    8 |
                     |              |
      +--------------+--------------+--------------+--------------+
      |              |              |              |              |
      |  9   10   11 | 17   18   19 | 25   26   27 | 33   34   35 |
      |              |              |              |              |
      | 12  left  13 | 20 front  21 | 28 right  29 | 36  rear  37 |
      |              |              |              |              |
      | 14   15   16 | 22   23   24 | 30   31   32 | 38   39   40 |
      |              |              |              |              |
      +--------------+--------------+--------------+--------------+
                     |              |
                     | 41   42   43 |
                     |              |
                     | 44 bottom 45 |
                     |              |
                     | 46   47   48 |
                     |              |
                     +--------------+

```
(cf. [Analyzing Rubik's Cube with GAP](https://www.gap-system.org/Doc/Examples/rubik.html))

# Index-Orientation

## Corners

### Index

```
0   1   2   3   4   5   6   7
ULB URB URF ULF DLF DLB DRB DRF
RBY RGY RGW RBW OBW OBY OGY OGW
```

### Orientation

0: oriented
1: rotated 120°
2: rotated 240°

## Edges

### Index

```
0  1  2  3  4  5  6  7  8  9  10 11
UB UR UF UL FR FL BL BR DF DL DB DR
RY RG RW RB WG WB YB YG OW OB OY OG
```

### Orientation

http://cube.rider.biz/zz.php?p=eoline#eo_detection

- oriented: 0
- flipped: 1
