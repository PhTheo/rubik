#! /usr/bin/env python3
# -*- coding: utf-8 -*-

""" Rubik's 3x3x3 cube """

from random import choices

# Positions:
"""
                     +--------------+
                     |              |
                     |  1    2    3 |
                     |              |
                     |  4  top    5 |
                     |              |
                     |  6    7    8 |
                     |              |
      +--------------+--------------+--------------+--------------+
      |              |              |              |              |
      |  9   10   11 | 17   18   19 | 25   26   27 | 33   34   35 |
      |              |              |              |              |
      | 12  left  13 | 20 front  21 | 28 right  29 | 36  rear  37 |
      |              |              |              |              |
      | 14   15   16 | 22   23   24 | 30   31   32 | 38   39   40 |
      |              |              |              |              |
      +--------------+--------------+--------------+--------------+
                     |              |
                     | 41   42   43 |
                     |              |
                     | 44 bottom 45 |
                     |              |
                     | 46   47   48 |
                     |              |
                     +--------------+

"""

def face(offset_x, offset_y):
    xpos = [ i + 3*offset_x for i in [1, 2, 3, 1, 3, 1, 2, 3] ]
    ypos = [ j + 3*offset_y for j in [3, 3, 3, 2, 2, 1, 1, 1] ]
    return list(zip(xpos, ypos))

# 1st col: https://colorswall.com/palette/171/
# 2nd col: http://ruwix.com (gimp color picker)
RED    = "#b71234"    # "#b80000"
BLUE   = "#0046ad"    # "#193bff"
YELLOW = "#ffd500"    # "#fee735"
GREEN  = "#009b48"    # "#00a85b"
ORANGE = "#ff5800"    # "#f77113"
WHITE  = "#ffffff"

COLORS = [RED, BLUE, YELLOW, GREEN, ORANGE, WHITE]

# FACES
F_TOP    = face(1, 2)
F_LEFT   = face(0, 1)
F_FRONT  = face(1, 1)
F_RIGHT  = face(2, 1)
F_REAR   = face(3, 1)
F_BOTTOM = face(1, 0)

POSITIONS = F_TOP + F_LEFT + F_FRONT + F_RIGHT + F_REAR + F_BOTTOM

CLEAN = [WHITE for _ in range(8)] + [ORANGE for _ in range(8)] + [GREEN for _ in range(8)] \
        + [RED for _ in range(8)] + [BLUE for _ in range(8)] + [YELLOW for _ in range(8)]

def draw_tile(graph, pos, col):
    i, j = pos
    graph.DrawRectangle((i-1, j-1), (i, j), fill_color=col)
    

def draw_tiles(graph, permutation=CLEAN):
    draw_tile(graph, ( 5,8), WHITE)
    draw_tile(graph, ( 2,5), ORANGE)
    draw_tile(graph, ( 5,5), GREEN)
    draw_tile(graph, ( 8,5), RED)
    draw_tile(graph, (11,5), BLUE)
    draw_tile(graph, ( 5,2), YELLOW)
    for pos, col in zip(POSITIONS, permutation):
        draw_tile(graph, pos, col)

# Permutations

def decycle(cycles):
    n = len(CLEAN)
    permutation = list(range(n))
    for cycle in cycles:
        m = len(cycle)
        for i in range(m):
            permutation[cycle[(i + 1) % m] - 1] = cycle[i] - 1
    return permutation

def permutate(cube, permutation):
    n = len(cube)
    return [ cube[permutation[i]] for i in range(n) ]

C_LEFT  = [[ 1, 17, 41, 40], [ 4, 20, 44, 37], [ 6, 22, 46, 35], [ 9, 11, 16, 14], [10, 13, 15, 12]]
C_RIGHT = [[ 3, 38, 43, 19], [ 5, 36, 45, 21], [ 8, 33, 48, 24], [25, 27, 32, 30], [26, 29, 31, 28]]
C_FRONT = [[ 6, 25, 43, 16], [ 7, 28, 42, 13], [ 8, 30, 41, 11], [17, 19, 24, 22], [18, 21, 23, 20]]
C_BACK  = [[ 1, 14, 48, 27], [ 2, 12, 47, 29], [ 3,  9, 46, 32], [33, 35, 40, 38], [34, 37, 39, 36]]
C_UP    = [[17,  9, 33, 25], [18, 10, 34, 26], [19, 11, 35, 27], [ 1,  3,  8,  6], [ 2,  5,  7,  4]]
C_DOWN  = [[22, 30, 38, 14], [23, 31, 39, 15], [24, 32, 40, 16], [41, 43, 48, 46], [42, 45, 47, 44]]

LEFT    = decycle(C_LEFT)
RIGHT   = decycle(C_RIGHT)
FRONT   = decycle(C_FRONT)
BACK    = decycle(C_BACK)
UP      = decycle(C_UP)
DOWN    = decycle(C_DOWN)

LEFT2   = permutate(LEFT, LEFT)
RIGHT2  = permutate(RIGHT, RIGHT)
FRONT2  = permutate(FRONT, FRONT)
BACK2   = permutate(BACK, BACK)
UP2     = permutate(UP, UP)
DOWN2   = permutate(DOWN, DOWN)

LEFT3   = permutate(LEFT2, LEFT)
RIGHT3  = permutate(RIGHT2, RIGHT)
FRONT3  = permutate(FRONT2, FRONT)
BACK3   = permutate(BACK2, BACK)
UP3     = permutate(UP2, UP)
DOWN3   = permutate(DOWN2, DOWN)

MDICT   = {"L": LEFT,  "L2": LEFT2,  "L'": LEFT3, 
           "R": RIGHT, "R2": LEFT2,  "R'": RIGHT3,
           "F": FRONT, "F2": FRONT2, "F'": FRONT3,
           "B": BACK,  "B2": BACK2,  "B'": BACK3,
           "U": UP,    "U2": UP2,    "U'": UP3,
           "D": DOWN,  "D2": DOWN2,  "D'": DOWN3}

GENERATOR   = ["L",  "R",  "F",  "B",  "U",  "D",
               "L'", "R'", "F'", "B'", "U'", "D'",
               "L2", "R2", "F2", "B2", "U2", "D2"]

def scramble(n=40):
    cube = CLEAN
    moves = choices(GENERATOR, k=n)
    for m in moves:
        cube = permutate(cube, MDICT[m])
    return(cube, moves)
