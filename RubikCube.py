#! /usr/bin/env python3
# -*- coding: utf-8 -*-

""" simulate rubik's 3x3x3 cube """

import PySimpleGUI as sg

import cube333 as c

margin = 0.05
graph = sg.Graph(canvas_size=(600,450), 
                 graph_bottom_left=(-margin, -margin), 
                 graph_top_right=(12+margin, 9+margin), 
                 background_color='gray', key='graph')

bsize = (5, 1)
layout = [ [sg.Text("Rubik's 3x3x3-Cube")],
           [graph],
           [sg.Button("L",  size=bsize), sg.Button("R",  size=bsize), sg.Button("F",  size=bsize), 
            sg.Button("B",  size=bsize), sg.Button("U",  size=bsize), sg.Button("D",  size=bsize)],
           [sg.Button("L'", size=bsize), sg.Button("R'", size=bsize), sg.Button("F'", size=bsize), 
            sg.Button("B'", size=bsize), sg.Button("U'", size=bsize), sg.Button("D'", size=bsize)],
           [sg.Button("L2", size=bsize), sg.Button("R2", size=bsize), sg.Button("F2", size=bsize), 
            sg.Button("B2", size=bsize), sg.Button("U2", size=bsize), sg.Button("D2", size=bsize)],
           [sg.Button('Reset'), sg.Button('Scramble'), sg.Button('Exit')],
           [sg.InputText(size=(85,1), focus=True, key='-STATUS-')]
]

window = sg.Window('3x3x3', layout)
window.Finalize()
cube = c.CLEAN
history= []
c.draw_tiles(graph, cube)

while True:
    event, values = window.read()
    if event == sg.WIN_CLOSED or event == 'Exit':
        break
    if event == "Reset":
        cube = c.CLEAN
        history = []
    if event == "Scramble":
        cube, moves = c.scramble(n=20)
        print(cube)
        history = moves
    if event == "R":
        history.append(event)
        cube = c.permutate(cube, c.RIGHT)
    if event == "L":
        history.append(event)
        cube = c.permutate(cube, c.LEFT)
    if event == "F":
        history.append(event)
        cube = c.permutate(cube, c.FRONT)
    if event == "B":
        history.append(event)
        cube = c.permutate(cube, c.BACK)
    if event == "U":
        history.append(event)
        cube = c.permutate(cube, c.UP)
    if event == "D":
        history.append(event)
        cube = c.permutate(cube, c.DOWN)
    if event == "R2":
        history.append(event)
        cube = c.permutate(cube, c.RIGHT2)
    if event == "L2":
        history.append(event)
        cube = c.permutate(cube, c.LEFT2)
    if event == "F2":
        history.append(event)
        cube = c.permutate(cube, c.FRONT2)
    if event == "B2":
        history.append(event)
        cube = c.permutate(cube, c.BACK2)
    if event == "U2":
        history.append(event)
        cube = c.permutate(cube, c.UP2)
    if event == "D2":
        history.append(event)
        cube = c.permutate(cube, c.DOWN2)
    if event == "R'":
        history.append(event)
        cube = c.permutate(cube, c.RIGHT3)
    if event == "L'":
        history.append(event)
        cube = c.permutate(cube, c.LEFT3)
    if event == "F'":
        history.append(event)
        cube = c.permutate(cube, c.FRONT3)
    if event == "B'":
        history.append(event)
        cube = c.permutate(cube, c.BACK3)
    if event == "U'":
        history.append(event)
        cube = c.permutate(cube, c.UP3)
    if event == "D'":
        history.append(event)
        cube = c.permutate(cube, c.DOWN3)

    c.draw_tiles(graph, cube)
    print(history)
    window['-STATUS-'].update(value= history)

window.close()
